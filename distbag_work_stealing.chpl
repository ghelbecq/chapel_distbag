use DistributedBag;

config const n = 2000;

var bag = new DistBag(int, targetLocales=Locales); // creation of the bag

bag.addBulk(1..n); // insertion of elements

writeln("Initial bag size: ", bag.getSize(), "\n");

coforall loc in Locales do on loc do {
  var counter: atomic int = 0; // local counter

  coforall tid in 0..#here.maxTaskPar do {

    while true {
      var (empty, x): (bool, int) = bag.remove(); // we try to remove an element
      if (empty == true){ // if we successfully removed an element
        counter.add(1); // update the counter
      }

      if (bag.getSize() == 0){ // if the global bag is empty
        break;
      }
    }

  } // end coforall threads

  writeln(counter, " element(s) removed from ", here);
} // end coforall locales
