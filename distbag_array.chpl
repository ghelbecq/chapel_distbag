use DistributedBag;

// TEST 1
var arr: [0..4] int = [0, 1, 2, 3, 4];
var bag1 = new DistBag([0..4] int, targetLocales=Locales);
bag1.add(arr); // RETURN AN ERROR

// TEST 2
record Node {
  var x: int;
  var y: [0..4] int;
}

var n = new Node(0, [0, 1, 2, 3, 4]);
var bag2 = new DistBag(Node, targetLocales=Locales);
bag2.add(n); // RETURN AN ERROR
