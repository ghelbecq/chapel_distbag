#include "../include/simple_bound.h"
#include "../include/aux.h"
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

int c_temps_s[_MAX_MCHN_*_MAX_JOBS_];
int minTempsDep_s[_MAX_MCHN_]; //read only
int minTempsArr_s[_MAX_MCHN_]; //read only -- fill once and fire

// int evalsolution(const int permutation[], const int machines, const int jobs, const int *times)
// {
//     int temp[jobs];
//
//  	  for (int mm = 0; mm < machines; mm++) temp[mm] = 0;
//
//  	  for (int j = 0; j < jobs; j++){
//    	  int job = permutation[j];
//       temp[0] += times[0*jobs + job];
//
//    		for (int m = 1; m < machines; m++){
//         temp[m] = max(temp[m], temp[m-1]) + times[m*jobs + job];
//       }
//     }
//
//    	return temp[machines-1];
// }

// Compute the time required between starting the first back-scheduled job on the each machine
// and the end of operations on the last machine
void scheduleBack(int *permut, int limit2, const int machines, const int jobs,
     int *minTempsDep_s, int *back, const int *times)
{
    int job;

    if (limit2 == jobs){
        for (int i = 0; i < machines; i++) back[i] = minTempsDep_s[i];
        return;
    }

    for (int j = 0; j < machines; j++) back[j] = 0;

    //reverse schedule (allowing incremental update)
    for (int k = jobs-1; k >= limit2; k--){
        job = permut[k];
        back[machines-1] += times[(machines-1)*jobs + job];

        for (int j = machines-2; j >= 0; j--){
            back[j] = max(back[j], back[j+1]) + times[j*jobs + job];
        }
    }
}

// Compute the completion time of the last front-scheduled job on each machine
void scheduleFront(int *permut, int limit1, const int machines, const int jobs,
  int *minTempsArr_s, int *front, const int *times)
{
    int job;

    if (limit1 == -1){
        for (int i = 0; i < machines; i++) front[i] = minTempsArr_s[i];
        return;
    }

    for (int i = 0; i < machines; i++) front[i] = 0;

    for (int i = 0; i < limit1+1; i++){
        job = permut[i];
        front[0] += times[0*jobs + job];

        for (int j = 1; j < machines; j++){
            front[j] = max(front[j-1], front[j]) + times[j*jobs + job];
        }
    }
}

// Compute the total remaining time of each unscheduled job on each machine
void sumUnscheduled(const int *permut, int limit1, int limit2, const int machines,
  const int jobs, int *remain, const int *times)
{
    int job;

    for (int j = 0; j < machines; j++) remain[j] = 0;

    for (int k = limit1+1; k < limit2; k++){
        job = permut[k];
        for (int j = 0; j < machines; j++){
            remain[j] += times[j*jobs + job];
        }
    }
}

// Compute the simple lowerbound
// int simple_bornes_calculer(int permutation[], int limite1, int limite2,
//     const int machines, const int jobs, int *minTempsArr, int *minTempsDep, const int *times)
// {
//     int front[machines];
//     int back[machines];
//     int remain[machines];
//
//     scheduleFront(permutation, limite1, machines, jobs, minTempsArr, front, times);
//     scheduleBack(permutation, limite2, machines, jobs, minTempsDep, back, times);
//     sumUnscheduled(permutation, limite1, limite2, machines, jobs, remain, times);
//
//     int lb;
//     int tmp0, tmp1, cmax;
//
//     tmp0 = front[0] + remain[0];
//     lb = tmp0 + back[0];
//
//     for (int j = 1; j < machines; j++){
//         tmp1 = front[j] + remain[j];
//         tmp1 = max(tmp1, tmp0);
//         cmax = tmp1 + back[j];
//         lb   = max(lb, cmax);
//         tmp0 = tmp1;
//     }
//
//     return lb;
// }

// Compute the simple lowerbound
void simple_bornes_calculer(int permutation[], int limite1, int limite2, const int machines,
  const int jobs, int *minTempsArr_s, int *minTempsDep_s, const int *times, int *lbs, const int side)
{
    int job;
    int front_p[machines];
    int back_p[machines];
    int remain_p[machines];

    int front_c[machines];
    int back_c[machines];
    int remain_c[machines];

    // Computation of {front, back, remain} of the parent
    scheduleFront(permutation, limite1, machines, jobs, minTempsArr_s, front_p, times);
    scheduleBack(permutation, limite2, machines, jobs, minTempsDep_s, back_p, times);
    sumUnscheduled(permutation, limite1, limite2, machines, jobs, remain_p, times);

    for (int i = limite1+1; i < limite2; i++){ // for each child
      swap(&permutation[limite1+1], &permutation[i]);
      job = permutation[limite1+1];

      // Deduction of {front, back, remain} of the child
      for (int j = 0; j < machines; j++){
        front_c[j] = front_p[j];
        back_c[j]  = back_p[j];
      }

      if (side == 0){ // if foreward
        front_c[0] = front_p[0] + times[0*jobs + job];
        for (int j = 1; j < machines; j++){
          front_c[j] = max(front_c[j], front_c[j-1]) + times[j*jobs + job];
        }
      }

      for (int j = 0; j < machines; j++){
        remain_c[j] = remain_p[j] - times[j*jobs + job];
      }

      if (side == 1){ // if backward
        back_c[machines-1] = back_p[machines-1] + times[(machines-1)*jobs + job];
        for (int j = machines-2; j >= 0; j--){
          back_c[j] = max(back_c[j], back_c[j+1]) + times[j*jobs + job];
        }
      }

      // Computation of the lowerbound
      int lb;
      int tmp0, tmp1, cmax;

      tmp0 = front_c[0] + remain_c[0];
      lb = tmp0 + back_c[0];

      for (int j = 1; j < machines; j++){
          tmp1 = front_c[j] + remain_c[j];
          tmp1 = max(tmp1, tmp0);
          cmax = tmp1 + back_c[j];
          lb   = max(lb, cmax);
          tmp0 = tmp1;
      }

      lbs[i-(limite1+1)] = lb;
    }

    // for (int i=0; i<limite2-(limite1+1); i++){
    //   printf("%d\t", lbs[i]);
    // }
    // printf("\n");
}

// void simple_bound_search(int machines, int jobs, int *times)
// {
//     register unsigned int flag = 0;
//     register int bit_test = 0;
//     register int i, depth; //para dizer que 0-1 ja foi visitado e a busca comeca de 1, bote 2
//     register unsigned long long int local_tree = 0ULL;
//     int num_sol = 0;
//     int lowerbound = 0;
//     int incumbent = 1713;
//     register int p1;
//
//     int minTempsDep[machines]; //read only
//     int minTempsArr[machines]; //read only -- fill once and fire
//
//     int permutation[jobs];//
//
//     int scheduled[jobs]; //flag
//     int position[jobs]; //position -- swap stuff:
//
//     remplirTempsArriverDepart(minTempsArr, minTempsDep, machines, jobs, times);
//
//     start_vector(position, jobs);
//     start_vector(permutation, jobs);
//
//     for (i = 0; i < jobs; ++i){
//         scheduled[i] = -1;
//     }
//
//     depth = 0;
//
//     do{
//         scheduled[depth]++;
//         bit_test = 0;
//         bit_test |= (1<<scheduled[depth]);
//
//         if (scheduled[depth] == jobs){
//             scheduled[depth] = -1;
//         } else {
//                 //se colocar aqui serao invalidas
//                 if (!(flag & bit_test)){ //is valid
//
//                     p1 = permutation[depth];
//                     swap(&permutation[depth], &permutation[position[scheduled[depth]]]);
//                     swap(&position[scheduled[depth]], &position[p1]);
//
//                     lowerbound = simple_bornes_calculer(permutation, depth, jobs,
//                          machines, jobs, minTempsArr, minTempsDep, times);
//
//                     if (lowerbound < incumbent){ // is it feasible
//
//                         flag |= (1ULL<<scheduled[depth]);
//                         depth++;
//                         local_tree++;
//
//                         if (depth == jobs){ //handle solution
//                             if (lowerbound < incumbent){
//                                 num_sol++;
//                                 incumbent = lowerbound;
//                                 printf("\n");
//                                 printf("\nnew incumbent solution: %d.\n",lowerbound);
//                                 print_permutation(permutation, jobs);
//                             }
//                         } else continue; // not a complete one
//                     } else continue; //not feasible
//                 } else continue; //not valid
//         } //else
//
//         depth--;
//         flag &= ~(1ULL<<scheduled[depth]);
//
//     } while(depth >= 0);
//
//     printf("\nNumber of solutions found: %d.\n", num_sol);
//     printf("\nTree size: %llu.\n", local_tree);
//     printf("\n\tBest solution: %d.\n", incumbent);
// }

// void simple_bound_call_search(short p)
// {
//     int jobs;
//     int machines;
//     int *times = get_instance(&machines, &jobs, p);
//     print_instance(machines, jobs, times);
//     simple_bound_search(machines, jobs, times);
// }
