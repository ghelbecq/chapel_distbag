use CTypes;

// Compilation constant: size of the problem
param JOBS: int = 20;
// Runtime constant: are the leaves printed ?
config const display: bool = false;

// Node record
record Node
{
  /* var size: int;       // size of the problem (= JOBS, can be removed ?) */
  var depth: int;         // depth
  var limit1: int;        // right limit
  var limit2: int;        // left limit
  var prmu: (JOBS*int);   // permutation

  // default initializer
  proc init()
  {
    /* this.size = JOBS; */
    this.depth  = 0;
    this.limit1 = -1;
    this.limit2 = JOBS;
    var tmp: JOBS*int;
    for i in 0..JOBS-1 do {
      tmp(i) = i;
    }
    this.prmu = tmp;
  }

  // copy initializer
  proc init(from: Node)
  {
    /* this.size = from.size; */
    this.depth  = from.depth;
    this.limit1 = from.limit1;
    this.limit2 = from.limit2;
    this.prmu   = from.prmu;
  }
}

// Convert a Chapel tuple to a C array
proc tupleToCptr(t: JOBS*int): c_ptr(c_int)
{
  var p: c_ptr(c_int) = c_malloc(c_int, JOBS);
  for i in 0..JOBS-1 {
    p[i] = t[i]:c_int;
  }
  return p;
}

// Print a leaf
proc printLeaf(n: Node): void
{
  if display {
    writeln(n.depth, " -- ", JOBS, ", ", n.prmu);
  }
}

// Return the best solution known of the given Taillard instance
proc chooseOptimal(instance: int): int
{
  var optimal: [1..120] int =  [1278, 1359, 1081, 1293, 1235, 1195, 1234, 1206, 1230, 1108,            // 20x5
                                1582, 1659, 1496, 1377, 1419, 1397, 1484, 1538, 1593, 1591,            // 20x10
                                2297, 2099, 2326, 2223, 2291, 2226, 2273, 2200, 2237, 2178,            // 20x20

                                2724, 2834, 2621, 2751, 2863, 2829, 2725, 2683, 2552, 2782,            // 50x5
                                2991, 2867, 2839, 3063, 2976, 3006, 3093, 3037, 2897, 3065,            // 50x10
                                3850, 3704, 3640, 3723, 3611, 3681, 3704, 3691, 3743, 3756,            // 50x20

                                5493, 5268, 5175, 5014, 5250, 5135, 5246, 5094, 5448, 5322,            // 100x5
                                5770, 5349, 5676, 5781, 5467, 5303, 5595, 5617, 5871, 5845,            // 100x10
                                6202, 6183, 6271, 6269, 6314, 6364, 6268, 6401, 6275, 6434,            // 100x20

                                10862, 10480, 10922, 10889, 10524, 10329, 10854, 10730, 10438, 10675,  // 200x10
                                11195, 11203, 11281, 11275, 11259, 11176, 11360, 11334, 11192, 11284,  // 200x20

                                26040, 26520, 26371, 26456, 26334, 26477, 26389, 26560, 26005, 26457]; // 500x20

  if ((instance < 1) || (instance > 120)){
    writeln("Unknown Taillard instance");
    return 0;
  } else {
    return optimal[instance];
  }
}

// Take a boolean array and return false if it contains at least one "true", "true" if not
proc isArrFalse(const arr: [] atomic bool): bool
{
  var res: bool = true;

  for elt in arr do {
    if (elt.read() == true){
      res = false;
      break;
    }
  }

  return res;
}
