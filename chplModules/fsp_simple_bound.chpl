module fsp_simple_bound
{
  use List;
  use Time;
  use CTypes;
  use DistributedBag;

  use fsp_node;
  use statistics;
  use fsp_aux_mlocale;
  use fsp_simple_aux_mlocale;
  use fsp_simple_chpl_c_headers;

  // Decompose a parent node and return the list of its feasible child nodes
  proc decompose(const parent: Node, tree_loc: atomic uint(64), const jobs: c_int, const machines: c_int,
    minTempsArr_s: c_ptr(c_int), minTempsDep_s: c_ptr(c_int), c_temps_s: c_ptr(c_int), side: int, incumbent_g: atomic uint(64)): list
  {
    var childList: list(Node); // list containing the child nodes

    var incumbent_l: uint(64) = incumbent_g.read();

    // Computation of lowerbounds
    var lowerbounds: c_ptr(c_int) = c_malloc(c_int, parent.limit2-(parent.limit1+1));
    var c_prmu: c_ptr(c_int) = tupleToCptr(parent.prmu);

    simple_bornes_calculer(c_prmu, parent.limit1:c_int, parent.limit2:c_int,
      machines, jobs, minTempsArr_s, minTempsDep_s, c_temps_s, lowerbounds, side:c_int);

    c_free(c_prmu);

    // Treatment of childs
    for i in parent.limit1+1..parent.limit2-1 do {

      if (parent.depth + 1 == jobs){ // if child leaf
        if (lowerbounds[i-(parent.limit1+1)] < incumbent_l){ // if child feasible
          incumbent_g.write(lowerbounds[i-(parent.limit1+1)]:uint(64));
        }
      } else { // if not leaf
        if (lowerbounds[i-(parent.limit1+1)] < incumbent_l){ // if child feasible
          var child = new Node(parent);
          child.depth += 1;

          if (side == 0){ // if forward
            child.limit1 += 1;
            child.prmu[child.limit1] <=> child.prmu[i];
          } else if (side == 1){ // if backward
            child.limit2 -= 1;
            child.prmu[child.limit2] <=> child.prmu[i];
          }

          childList.append(child);
          tree_loc.add(1);
        }
      }

    } // for childs

    c_free(lowerbounds);

    return childList;
  }

}
