use fsp_simple_bound;
use fsp_simple_chpl_c_headers;
use fsp_aux_mlocale;
use fsp_simple_aux_mlocale;
use DistributedBag;
use fsp_node;
use CTypes;
use Time;
use List;

// FSP instance
config const instance: int(8) = 7; // 1 ~ 120
// FSP branching rule
config const side: int = 0; // front (0), back (1)
// Size of active set
config const initSize: int = 2000;

proc fsp_simple_search(const instance: int(8), const initSize: int, const side: int): void
{
  var incumbent_g: atomic uint(64) = chooseOptimal(instance):uint(64);

  var jobs, machines: c_int;
  var times: c_ptr(c_int) = get_instance(machines, jobs, instance);
  print_instance(machines, jobs, times);

  var local_times: [0..(machines*jobs)-1] c_int = [i in 0..(machines*jobs)-1] times[i];

  //initialization
  fsp_simple_all_locales_get_instance(local_times, machines, jobs);
  fsp_simple_all_locales_init_data(machines, jobs); // We fill minTempsArr, minTempsDep

  /*
  Step 1: we fill a list with a sufficiently large number of nodes (initSize),
  before insertion in the distributed bag. This is done sequentially.
  REMARK: we didn't use bag directly because we need an ordered data structure to
  ensure the good exploration of the tree (breadth-first).
  */

  var initList: list(Node);
  var root = new Node();
  initList.append(root);

  var active_set_size: atomic uint(64) = 0:uint(64);

  // Exploration of the tree
  while (initList.size < initSize) do {
    var parent: Node = initList.pop();

    {
      var childList: list(Node) = decompose(parent, active_set_size, jobs, machines,
        minTempsArr_s, minTempsDep_s, c_temps_s, side, incumbent_g);

      for elt in childList do {
        initList.insert(0, elt);
      }
    }

  } // end while

  writeln("Step 1: OK");

  /*
  Step 2: Nodes are inserted in the distributed bag and quasi-uniformly distributed
  across locales. At this point, the exploration of the tree is done in parallel.
  */

  // Creation and initialization of the distributed bag
  var bag = new DistBag(Node, targetLocales=Locales);
  bag.addBulk(initList);
  initList.clear();
  bag.balance();

  coforall loc in Locales do on loc do {
    var tree_loc: atomic uint(64) = 0:uint(64);
    var timer: Timer;

    var arr_thr: [0..#here.maxTaskPar] atomic bool = true;

    timer.start();
    coforall threadId in 0..#here.maxTaskPar do {

      // Exploration of the tree
      while true do {

        var (notEmpty, parent): (bool, Node) = bag.remove();

            // Termination condition
          if !notEmpty { // if locally empty
            arr_thr[threadId].write(false);
            if isArrFalse(arr_thr) { // if globaly empty
              break;
            }
            continue;
          } else { // if locally not empty
            arr_thr[threadId].write(true);
          }

        {
          var childList: list(Node) = decompose(parent, tree_loc, jobs, machines, minTempsArr_s,
            minTempsDep_s, c_temps_s, side, incumbent_g);

          bag.addBulk(childList);
        }

      } // end while

    } // end coforall tasks
    timer.stop();

    writeln("on ", here, ", ", tree_loc, " nodes processed in ", timer.elapsed(TimeUnits.seconds), " seconds.");

  } // end coforall locales
}

proc main(): int
{
  fsp_simple_search(instance, initSize, side);
  return 0;
}
