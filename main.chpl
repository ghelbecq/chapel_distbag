use fsp_simple_bound;

// FSP instance
config const instance: int(8) = 7; // 1 ~ 120
// FSP branching rule
config const side: int = 0; // front (0), back (1)
// Size of active set
config const initSize: int = 2000;

proc main(): int
{
  fsp_simple_search(instance, initSize, side);
  return 0;
}
