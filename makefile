SHELL := /bin/bash

BUILD_DIR := ./bin
CHPL_MODULES_DIR := ./chplModules

chapel: dir
	@echo " ### Building the Chapel code ### "
	@echo

	chpl -M $(CHPL_MODULES_DIR) --fast distbag_processing.chpl -o $(BUILD_DIR)/distbag_processing.o
	chpl -M $(CHPL_MODULES_DIR) --fast distbag_array.chpl -o $(BUILD_DIR)/distbag_array.o
	chpl -M $(CHPL_MODULES_DIR) --fast distbag_work_stealing.chpl -o $(BUILD_DIR)/distbag_work_stealing.o

	@echo
	@echo " ### Compilation done ### "

dir:
	@echo
	@echo " ### creating directories ### "
	@echo
	mkdir -p $(BUILD_DIR)

.PHONY: clean
clean:
	$(RM) $(BUILD_DIR)/*.o
	$(RM) $(BUILD_DIR)/*.o_real
